if ( typeof window.toDoList === 'undefined') {
	window.toDoList = {};
}

window.toDoList.ui = (function($, model, utils) {

	var mainPageInit = function() {
		loadTasksListFromModel();
	};

	var loadTasksListFromModel = function() {
		var groupedTasks = model.getTasksGroupedByPriorities();
		var tasksList = $('#tasksList');
		tasksList.empty();

		var prioritiesOrder = model.getPrioritiesIdArray().sort().reverse();
		prioritiesOrder.forEach(function(priorityId) {
			var priorityName = model.getPriorityNameById(priorityId);
			var listDivider = createListDivider(priorityName);
			tasksList.append(listDivider);
			var tasksOfPriority = groupedTasks[priorityId];
			tasksOfPriority.forEach(function(task) {
				var taskListEntry = createTaskListEntry(task);
				tasksList.append(taskListEntry);
			});
		});

		tasksList.listview('refresh');
	};

	var createListDivider = function(priorityName) {
		var divider = $('<li>');
		divider.jqmData('role', 'list-divider');
		divider.append(priorityName + ' priority');
		return divider;
	};
	var createTaskListEntry = function(task) {
		var taskListEntry = $('<li>');
		taskListEntry.attr('data-task-id', task.getId());
		taskListEntry.append('<a href="#">' + task.getName() + '</a>');

		taskListEntry.on('vclick', function() {
			var isInAcceptState = $(this).attr('data-in-accept-state');
			if (isInAcceptState==='true') {
				deleteTask(task.getId());
			} else {
				$.mobile.changePage('editTask.html', {
					data : {
						taskId : task.getId()
					}
				});
			}
		});

		taskListEntry.on('swiperight', function() {
			$(this).attr('data-in-accept-state', 'true');
			$(this).buttonMarkup({
				icon : "check"
			});
			$(this).addClass('strike');
		});

		taskListEntry.on('swipeleft', function() {
			$(this).attr('data-in-accept-state', 'true');
			$(this).buttonMarkup({
				icon : "arrow-r"
			});
			$(this).removeClass('strike');
		});
		return taskListEntry;
	};
	var addNewTaskPageInit = function() {
		throw "Brak obsługi przycisku Save";
	};
	var addNewTaskPageShow = function() {
		$('#taskName').focus();
	};
	var addNewTask = function(name, priorityId, description) {
		var task = model.addTask(name, priorityId, description);
		model.saveInLocalStorage();

		var newTaskListEntry = createTaskListEntry(task);
		loadTasksListFromModel();
		$.mobile.back();
	};

	var editTaskPageBeforeShow = function() {
		var getTaskIdFromUrl = function(url) {
			url = url.split("?")[1];
			return url.replace("taskId=", "");
		};
		var taskId = getTaskIdFromUrl($(this).jqmData("url"));
		$('#editTaskPage').jqmData("task-id", taskId);
		var task = model.getTaskById(taskId);

		fillEditTaskPageWithEditingTaskData(task);
	};

	var fillEditTaskPageWithEditingTaskData = function(task) {
		$('#taskName').val(task.getName());
		$('#taskPriority').val(task.getPriorityId());
		$('#taskPriority').selectmenu('refresh');
		$('#taskDescription').val(task.getDescription());
	};

	var editTaskPageInit = function() {
		throw "Przyciski Save oraz Button nie sa podlaczone";
	};

	var saveEditedTask = function() {
		var taskId = $('#editTaskPage').jqmData("task-id");
		var task = model.getTaskById(taskId);
		task.setName($('#taskName').val());
		task.setPriorityId($('#taskPriority').val());
		task.setDescription($('#taskDescription').val());
		model.saveInLocalStorage();
		loadTasksListFromModel();
	};

	var deleteEditedTask = function() {
		var taskId = $('#editTaskPage').jqmData("task-id");
		deleteTask(taskId);
	};
	
	var deleteTask = function(taskId){
		model.deleteTask(taskId);
		model.saveInLocalStorage();
		var deletedTaskListEntry = $("li[data-task-id='" + taskId + "']");
		deletedTaskListEntry.remove();
	};

	$(document).on('pageinit', '#mainPage', mainPageInit);
	$(document).on('pageinit', '#addNewTaskPage', addNewTaskPageInit);
	$(document).on('pageshow', '#addNewTaskPage', addNewTaskPageShow);
	$(document).on('pageinit', '#editTaskPage', editTaskPageInit);
	$(document).on('pagebeforeshow', '#editTaskPage', editTaskPageBeforeShow);

	model.loadFromLocalStorage();

})($, window.toDoList.model, window.toDoList.utils);
