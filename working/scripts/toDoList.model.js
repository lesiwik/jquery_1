if ( typeof window.toDoList === 'undefined') {
	window.toDoList = {};
}

window.toDoList.model = (function(utils) {
	var prioritiesMap = {
		1 : "Low",
		2 : "Normal",
		3 : "High"
	};

	var model = {
		tasks : {},
	};

	var newTask = function(id, name, priorityId, description) {
		if (name.trim() == '') {
			throw "Name cannot be empty";
		}

		return {
			getId : function() {
				return id;
			},
			getName : function() {
				return name;
			},
			getPriorityId : function() {
				return priorityId;
			},
			getPriorityName : function() {
				return prioritiesMap[priorityId];
			},
			getDescription : function() {
				return description;
			},
			setName : function(newName) {
				if (name.trim() == '') {
					throw "Name cannot be empty";
				}
				name = newName;
			},
			setPriorityId : function(newPriorityId) {
				if (!( newPriorityId in prioritiesMap))
					throw "Invalid priority Id";
				priorityId = newPriorityId;
			},
			setDescription : function(newDescription) {
				description = newDescription;
			},
			toJSON : function() {
				return {
					id : id,
					name : name,
					priorityId : priorityId,
					description : description
				};
			}
		};
	};

	var saveInLocalStorage = function() {
		localStorage.tasksList = JSON.stringify(window.toDoList.model);
	};

	var loadFromLocalStorage = function() {
		var tasksListFromStorage = localStorage.tasksList;
		if (tasksListFromStorage) {
			var modelFromStorage = JSON.parse(tasksListFromStorage);
			if (modelFromStorage) {
				modelFromStorage.tasks.forEach(function(taskFromStorage) {
					var task = newTask(taskFromStorage.id, taskFromStorage.name, taskFromStorage.priorityId, taskFromStorage.description);
					model.tasks[task.getId()] = task;
				});
			}
		}
	};

	var getTaskById = function(taskId) {
		return model.tasks[taskId];
	};

	var addTask = function(name, priorityId, description) {
		var taskId = utils.createGuid();
		var task = newTask(taskId, name, priorityId, description);
		model.tasks[taskId] = task;
		return task;
	};

	var deleteTask = function(taskId) {
		delete model.tasks[taskId];
	};

	var getTasksArray = function() {
		var tasks = [];
		for (var taskId in model.tasks) {
			tasks.push(model.tasks[taskId]);
		}
		return tasks;
	};

	var getTasksGroupedByPriorities = function() {
		var getTasksByPriorityId = function(priorityId) {
			var allTasks = getTasksArray();
			if (priorityId == null) {
				return allTasks;
			} else {
				return jQuery.grep(allTasks, function(task) {
					return task.getPriorityId() == priorityId;
				});
			}
		};

		var groupedTasks = {};
		for (var priorityId in prioritiesMap) {
			groupedTasks[priorityId] = getTasksByPriorityId(priorityId);
		}
		
		return groupedTasks;
	};

	var getPrioritiesIdArray = function(){
		var prioritiesIdArray = [];
		for(var priorityId in prioritiesMap)
			prioritiesIdArray.push(parseInt(priorityId));
			
		return prioritiesIdArray;
	};
	
	var getPriorityNameById = function(priorityId) {
		return prioritiesMap[priorityId];
	};

	return {
		addTask : addTask,
		deleteTask : deleteTask,
		getTaskById : getTaskById,
		getTasksArray : getTasksArray,
		getTasksGroupedByPriorities : getTasksGroupedByPriorities,
		loadFromLocalStorage : loadFromLocalStorage,
		saveInLocalStorage : saveInLocalStorage,
		getPriorityNameById : getPriorityNameById,
		getPrioritiesIdArray:getPrioritiesIdArray,
		toJSON : function() {
			return {
				tasks : getTasksArray()
			};
		}
	};
})(window.toDoList.utils);
